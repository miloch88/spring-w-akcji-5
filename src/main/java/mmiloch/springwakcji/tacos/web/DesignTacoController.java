package mmiloch.springwakcji.tacos.web;

import mmiloch.springwakcji.tacos.Ingredient;
import mmiloch.springwakcji.tacos.Ingredient.Type;
import mmiloch.springwakcji.tacos.Order;
import mmiloch.springwakcji.tacos.Taco;
import mmiloch.springwakcji.tacos.data.IngredientRepository;
import mmiloch.springwakcji.tacos.data.TacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

    private final IngredientRepository ingredientRepo;
    private TacoRepository tacoRepo;

    @Autowired
    public DesignTacoController(
            IngredientRepository ingredientRepo,
            TacoRepository tacoRepo) {
        this.ingredientRepo = ingredientRepo;
        this.tacoRepo = tacoRepo;
    }

    @ModelAttribute(name = "order")
    public Order order() {
        return new Order();
    }

    @ModelAttribute(name = "design")
    public Taco design() {
        return new Taco();
    }


    @GetMapping
    public String showDesignForm(Model model) {
        addIngredientsModels(model);

        return "design";
    }

    @PostMapping
    public String processDesign(
            @Valid  @ModelAttribute(name = "design") Taco taco, Errors errors,
            @ModelAttribute Order order, Model model) {

        addIngredientsModels(model);

        if (errors.hasErrors()) {
            return "design";
        }

        Taco saved = tacoRepo.save(taco);
        order.addDesign(saved);

        return "redirect:/orders/current";
    }

    private void addIngredientsModels(Model model) {
        List<Ingredient> ingredients = new ArrayList<>();
        ingredientRepo.findAll().forEach(i -> ingredients.add(i));

        Type[] types = Type.values();
        for (Type type : types) {
            model.addAttribute(type.toString().toLowerCase(),
                    filterByType(ingredients, type));
        }
    }

    private List<Ingredient> filterByType(
            List<Ingredient> ingredients, Type type) {
        return ingredients
                .stream()
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }

}
