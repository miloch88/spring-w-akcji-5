package mmiloch.springwakcji.tacos.data;

import mmiloch.springwakcji.tacos.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
