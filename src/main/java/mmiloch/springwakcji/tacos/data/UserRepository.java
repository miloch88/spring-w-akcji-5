package mmiloch.springwakcji.tacos.data;

import mmiloch.springwakcji.tacos.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
