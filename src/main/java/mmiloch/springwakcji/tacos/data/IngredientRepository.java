package mmiloch.springwakcji.tacos.data;

import mmiloch.springwakcji.tacos.Ingredient;
import org.springframework.data.repository.CrudRepository;

public interface IngredientRepository extends CrudRepository<Ingredient, String> {
}
