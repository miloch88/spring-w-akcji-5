package mmiloch.springwakcji.tacos.data;

import mmiloch.springwakcji.tacos.Taco;
import org.springframework.data.repository.CrudRepository;

public interface TacoRepository extends CrudRepository<Taco, Long> {
}
